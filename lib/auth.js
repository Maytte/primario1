import { AsyncStorage } from "react-native";
import Network from './rest';
import Globals from './globals';

export const onSignIn = async (email, password) => {
  try {
    var response = await Network.post(Globals.BASE_URL + '/user/login', {
      email: email,
      password: password
    });
    if (response.success && response.data && response.data.id) {
      await AsyncStorage.setItem('sessionToken', response.data.token);
    }
    return response;
  } catch (err) {
    console.log('algo salio mal en auth onSignIn');
    throw err;
  }
};

export const onSignOut = async () => {
  try {
    var response = await Network.get(Globals.BASE_URL + '/user/logout')
    return response.success;
  } catch (err) {
    console.log('algo salio mal en auth onSignOut');
    throw err;
  }
};

export const isSignedIn = async () => {
  try {
    var response = await Network.get(Globals.BASE_URL + '/user/sess')
    if (response.success) {
      return true;
    } else {
      return false;
    }
  } catch (err) {
    console.log('algo salio mal en auth isSignedIn');
    throw err;
  }
};