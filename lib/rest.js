import { AsyncStorage } from "react-native";

export default {
  get: async function (URL) {
    var token = await AsyncStorage.getItem('sessionToken');
    var headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    };
    if (token) {
      headers['authorization'] = token;
    }
    try {
      var response = await fetch(URL, {
        method: 'GET',
        headers: headers,
      });
      return response.json();
    } catch (err) {
      console.log(err);
    }
  },
  post: async function (URL, data) {
    var token = await AsyncStorage.getItem('sessionToken');
    var headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    };
    if (token) {
      headers['authorization'] = token;
    }
    var response = await fetch(URL, {
      method: 'POST',
      headers: headers,
      body: JSON.stringify(data)
    });
    return response.json();
  },
};