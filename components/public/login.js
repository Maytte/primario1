import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { onSignIn } from '../../../lib/auth'

class Login extends Component<{}> {

  constructor(props) {
    super(props);
    this.state = {}
  }

  onLogin(){
    this.props.showDrawer()
    Actions.landing({ type: "reset" })
  }

  render(){
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}
          onPress={ () => this.onLogin() }>
          Login
        </Text>
        <Text style={styles.welcome}
          onPress={ () => Actions.registro()}>
          Registro
        </Text>
        <Text style={styles.welcome}
          onPress={ () => Actions.recuperacion()}>
          Recuperacion
        </Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FF8200',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
  }
})

const mapStateToProps = state => {
  return { drawer: state.drawer }
}

export default connect(({routes}) => ({routes}))(Login)
