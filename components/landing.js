import React, { Component, PropTypes } from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

class Landing extends Component {

  render () {
    const {routes} = this.context;
    return (
      <View style={styles.outerContainer}>
        <Text>
          La scena actual es landing
        </Text>
        <Text onPress={Actions.example}>Ir a scena ejemplo</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  outerContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    flex: 1
  }
})

export default connect(state => ({
  routes: state.routes
}), null)(Landing);
