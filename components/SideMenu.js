import React, { Component } from 'react';
import { View, Text, ScrollView, Image, TouchableOpacity, StyleSheet } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { onSignOut } from "../../lib/auth";

export default class ControlPanel extends Component {
  constructor(props) {
    super(props);
  }

  reset() {
    this.props.hideDrawer();
    this.props.closeDrawer();
    Actions.login({ type: 'reset' })
  }

  onChange(scene){
    this.props.closeDrawer();
    switch (scene) {
      case 'landing':
        Actions.landing({ type: 'reset' })
      break;
      case 'example':
        Actions.example({ type: 'reset' })
      break;
    }
  }

  onLogout() {
    onSignOut()
      .then(res => { this.reset(); alert('cerraste session') })
      .catch(err => { this.reset(); alert("Error al cerrar sesión") });
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "white" }}>
        <View style={{ flex: 2, alignContent: 'center' }}>
          <Image source={require("../../assets/user.png")} style={styles.user}/>
          <Text style={{ fontSize: 20, fontWeight: 'bold', alignSelf: 'center' }}>
            UserName
          </Text>
        </View>
        <View style={{ flex: 3 }}>
          <View style={styles.separador}>
            <Text onPress={() => this.onChange('landing')} 
              style={styles.textMenu}>
              Inicio
            </Text>
            </View>
          <View style={styles.separador}>
            <Text onPress={() => this.onChange('example')}
              style={styles.textMenu}>
              Example
            </Text>
          </View>
        </View>
        <View style={{ flex: 1 }}>
          <TouchableOpacity onPress={() => this.onLogout()}>
            <View>
              <Image source={require("../../assets/logout.png")} 
                style={styles.logout} />
              <Text style={styles.textLogout}>
                Salir
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  user:{
    margin: 20,
    width: 100, 
    height: 100, 
    alignSelf: 'center'
  },
  separador: {
    borderColor: 'gray', 
    borderTopWidth: 0.5, 
    borderBottomWidth: 0.5
  },
  textMenu: {
    margin: 20, 
    fontSize: 20
  },
  logout: {
    width: 25,
    height: 25,
    margin: 10,
    alignSelf: 'center'
  },
  textLogout:{
    fontSize: 20, 
    fontWeight: 'bold', 
    alignSelf: 'center'
  }
});