/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,

  TouchableOpacity,
  TextInput,
  AppRegistry, 
  FlatList
} from 'react-native';
import { Router, Scene } from 'react-native-router-flux';
const store = configureStore()
const RouterWithRedux = connect()(Router);
const TabIcon = ({ selected, title }) => {
  return (
    <Text style={{ color: selected ? 'red' : 'black' }}>{title}</Text>
  )
}

type Props = {};
export default class App extends Component<Props> {
  constructor(props){
    super(props);
    this.state = {
      count: 0,
      text: '',
      data: [
        {key: '1'},
        {key: 'Jackson'},
        {key: 'James'},
      ]
    }
  }

  onClick(){
    this.setState({count: this.state.count+1})
  }
  onSave(){
    this.setState({count: this.state.count+1})
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.instructions}>
          {this.state.count}
        </Text>
       
        <TouchableOpacity onPress={this.onClick.bind(this)} style={styles.button}>
          <Text style={{textAlign: 'center', textAlignVertical: 'center',color: 'white'}}>Press</Text>
        </TouchableOpacity>

        <TextInput
          style={{height: 40, width: 100}}
          onChangeText={(text) => this.setState({text})}
          value={this.state.text}
        />
        <TouchableOpacity onPress={this.onSave.bind(this)} style={styles.button2}>
          <Text style={{textAlign: 'center', textAlignVertical: 'center',color: 'white'}}>Press</Text>
        </TouchableOpacity>

        <FlatList
          data={this.state.data}
          renderItem={({item}) => <Text style={styles.item}>{item.key}</Text>}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
   flex: 1,
   paddingTop: 22,
   justifyContent: 'center',
   alignItems: 'center',
   backgroundColor: '#F5FCFF'

  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  button: {
    borderRadius: 50,
    backgroundColor: 'purple', 
    width: 100, //'100%'
    height: 50
  },
  button2: {
    borderRadius: 50,
    backgroundColor: 'pink', 
    width: 100, //'100%'
    height: 50
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
});